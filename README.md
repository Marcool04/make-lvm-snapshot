# Make an LVM snapshot of root before the system is up

This collection of scripts is based on the instructions on the Archlinux Wiki
page [Create root filesystem snapshots with LVM](https://wiki.archlinux.org/title/Create_root_filesystem_snapshots_with_LVM).
This aims to: 'set up a root filesystem LVM snapshot creation during system start.
Such snapshots can be used for full system backups with minimal downtime or
testing system updates with the option to revert them.'

**NOTE: first of all make sure you have followed the setup outlined in the
`Prerequisites` section of that wiki page.**

The script that is run in early boot will:
- Copy `/boot` and/or `/efi` to folders on the root drive
- Make an LVM snapshot of the root LVM volume (with those copies in them)
- Reset the default target so next boot isn't a snapshot-creating one
- Start a monitor to alert the user in case the root snapshot fills up
- Get you into the default target which you initiated the reboot from


## Installation

Archlinux users can take advantage of [the AUR package](https://aur.archlinux.org/packages/make-lvm-snapshot):

```
$ git clone "https://aur.archlinux.org/make-lvm-snapshot.git"
$ cd make-lvm-snapshot
$ less PKGBUILD
$ makepkg -si
```

Manual installation involves something like this:

```
$ wget "https://gitlab.com/Marcool04/make-lvm-snapshot/-/archive/master/make-lvm-snapshot-master.zip"
$ unzip make-lvm-snapshot-master.zip
$ cd make-lvm-snapshot-master
$ su
Password:
# cp make-lvm-snapshot.sh monitor_lvm_snapshot_usage.sh snapshot-root-lvm-on-reboot.sh "/usr/bin"
# cp monitor_lvm_snapshot_usage.service "/usr/lib/systemd/system/"
# cp make-lvm-snapshot.target make-lvm-snapshot.service "/usr/lib/systemd/system/"
# chmod +x /usr/bin/make-lvm-snapshot.sh /usr/bin/monitor_lvm_snapshot_usage.sh /usr/bin/snapshot-root-lvm-on-reboot.sh
```


## Usage

To create a consistent lvm snapshot of the root volume, with a bonus backup of the
contents of `/boot` in `/boot_on_root` and `/efi` in `/efi_on_root` just do:

```
# snapshot-root-lvm-on-reboot.sh
```

and hope it all works! Of course, YMMV, and be aware of the differences between
your system's setup and the example which the script is designed for.


## Configuration

The scripts do their best to detect everything, but that may fail, and they
try to fail safely. In order to tweek things, you can set environment variables
in `/etc/default/make-lvm-snapshot`. Two important variables are the volume and
volume group (in an LVM sense) of your root partition:

```bash
MLS_ROOT_VOLUME_GROUP=
MLS_ROOT_VOLUME=
```

You can also optionally set the following options that will instruct the monitor
script to raise warnings then errors when the snapshot is filling up the available
space. These can be set to any integer between 1 and 99, with MLS_MONITOR_THRESHOLD (default: 50) strictly smaller than MLS_MONITOR_CRITICAL_THRESHOLD (default: 90). Also, MLS_MONITOR_CHECK_INTERVAL allows you to set the time that the monitor script waits between two checks (default: 120 seconds).

```bash
MLS_MONITOR_THRESHOLD=
MLS_MONITOR_CRITICAL_THRESHOLD=
MLS_MONITOR_CHECK_INTERVAL=
```



## Troubleshooting

After reboot, check if the snapshot was created or not with:

```
# lvs
```

To check for problems, you can take a look at the output of:

```
# journalctl -b -u make-lvm-snapshot.service
```

One problem you might face is that you might well end up in the wrong target.
This can easily be fixed with:

```
# systemctl isolate <your-desired-target>.target
```

For instance, if you are on a desktop system, and after using this script to
create a snapshot you end up at a terminal, with no desktop, just do:

```
# systemctl isolate graphical.target
```

Then, go back to configuration and follow the steps concerning the [Default target](#default-target).
