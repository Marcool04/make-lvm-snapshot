#!/usr/bin/env bash

set -eu

if (( UID != 0 )); then
  echo "FATAL: This script must be run with root privileges."
  exit 20
fi

usage() {
    echo "Usage:"
    echo "$0 [-r] [-?]"  1>&2
    echo "   -r   -> Reboot after setting up snapshot creation (default: no)"  1>&2
    echo "   -?   -> display this help message." 1>&2
}

monitor_service="monitor_lvm_snapshot_usage.service"
snapshot_target="make-lvm-snapshot.target"
snapshot_service="make-lvm-snapshot.service"

reboot="FALSE"
while getopts "r?" opt; do
  case $opt in
    r) reboot="TRUE"
    ;;
    ?) usage
       exit 0
    ;;
    *) echo "FATAL: Failed to parse command line options."
       usage
       exit 2
    ;;
  esac
done
shift $((OPTIND-1))

current_default_target="$(systemctl get-default)"
if [[ "$current_default_target" == "$snapshot_target" ]]; then
  echo "FATAL: The current default target $current_default_target is the snapshot_target."
  echo "       Somthing is wrong, you shouldn't be running this script now."
  exit 3
fi
old_required_target="$(systemctl show --property=Requires "$snapshot_target" |\
  sed -nr 's/^Requires=(.*)$/\1/p')"
if [[ "$old_required_target" == "$current_default_target" ]]; then
  echo "INFO: The required target is already the right one, no need to change it."
else
  echo "INFO: Changing current target to $current_default_target in override file."
  cat <<EOF > "/etc/systemd/system/$snapshot_target"
[Unit]
Description=Make LVM snapshot target (overriden)
Requires=$current_default_target
EOF
  systemctl daemon-reload
fi

new_required_target="$(systemctl show --property=Requires "$snapshot_target" |\
  sed -nr 's/^Requires=(.*)$/\1/p')"
if [[ "$new_required_target" != "$current_default_target" ]]; then
  echo "FATAL: failed to write new target to override file."
  exit 5
fi

echo "INFO: Setting target for next reboot to ${snapshot_target}."
if ! systemctl set-default "$snapshot_target"; then
    echo "FATAL: failed to set target to $snapshot_target"
    exit 7
fi

echo "INFO: Enabling snapshot service."
if ! systemctl enable "$snapshot_service"; then
  echo "FATAL: failed to enable the snapshot service."
  exit 11
fi

echo "INFO: Enabling monitor service."
if ! systemctl enable "$monitor_service"; then
  echo "WARN: failed to enable the monitor service."
  echo "      you will have an unmonitored snapshot after reboot."
fi

if [[ "$reboot" == "TRUE" ]]; then
  echo "INFO: Rebooting."
  systemctl reboot
fi

exit 0
