#!/usr/bin/env bash

set -eu

if [[ -t 0 ]]; then
    echo "FATAL: this script is meant to be called from a systemd service on boot."
    exit 100
fi
if (( UID != 0 )); then
    echo "This script must be run as root"
    exit 20
fi

snapshot_target="make-lvm-snapshot.target"

env_file="/etc/default/make-lvm-snapshot"
if [[ -e "$env_file" ]]; then
    # shellcheck source=/dev/null
    source "$env_file"
fi

volume_group="${MLS_ROOT_VOLUME_GROUP:-}"
volume="${MLS_ROOT_VOLUME:-}"
if [[ -z "$volume_group" ]] || [[ -z "$volume" ]]; then
    echo "Attempting to identify root volume and volume group..."
    root_device="$(findmnt -n -o SOURCE /)"
    vg_vol="${root_device//\/dev\/mapper\/}"
    vg="${vg_vol//-*}"
    vol="${vg_vol//*-}"
    readarray -t lvs_results_for_vg_vol < <(\
      lvs --reportformat json "/dev/${vg}/${vol}" | \
      jq --compact-output '.report[].lv[] | select(.origin != "")' \
    )
    if [[ "${#lvs_results_for_vg_vol[@]}" == 1 ]] && \
       [[ -b "/dev/${volume_group}/${volume}" ]]
    then
        volume="$vol"
        volume_group="$vg"
        echo "The system is booted on $volume volume on $volume_group volume group."
    else
        echo "FATAL: Failed to identify volume and volume group."
        exit 200
    fi
fi


###############
# BOOT and EFI
#
root_pre_snapshot_mnt="/mnt/for_snapshot_${volume_group}-${volume}"
echo -n "Mounting /dev/mapper/${volume_group}-${volume} to $root_pre_snapshot_mnt "
echo    "for /boot and /efi backup"
if ! mount --mkdir "/dev/mapper/${volume_group}-${volume}" "$root_pre_snapshot_mnt"; then
    echo -n "Failed to mount "
    echo    "/dev/mapper/${volume_group}-${volume} to $root_pre_snapshot_mnt"
    echo "when preparing to backup /boot and/or /efi. Skipping this stage"
    exit_code+=13
else
    fstab_boot_cnt="$(grep '/boot' /etc/fstab | awk '$0 !~ /^\s*#/ {print $1}' | wc -l )"
    if (( fstab_boot_cnt != 1 )); then
        echo "There are no conforming /boot lines in fstab."
        echo "Not working on backing up boot partition."
    else
        boot_device="$(grep '/boot' /etc/fstab | awk '$0 !~ /^\s*#/ {print $1}')"
        if [[ -n "$boot_device" ]]; then
            boot_mnt_pt="/mnt/tmp_mount_of_boot_for_backup"
            echo "Mounting boot device: $boot_device to $boot_mnt_pt"
            if mount --mkdir "$boot_device" "$boot_mnt_pt"; then
                echo "Creating backup of boot in ${root_pre_snapshot_mnt}/boot_on_root"
                rm -fR "${root_pre_snapshot_mnt}/boot_on_root"
                cp -a "$boot_mnt_pt" "${root_pre_snapshot_mnt}/boot_on_root"
                echo "Unmounting $boot_mnt_pt"
                umount "$boot_mnt_pt" && rm -R "$boot_mnt_pt"
            else
                echo -n "Boot device $boot_device did not mount, there will be no copy "
                echo "at /boot_on_root in snapshot"
                exit_code+=7
            fi
        fi
    fi
    fstab_efi_cnt="$(grep '/efi' /etc/fstab | awk '$0 !~ /^\s*#/ {print $1}' | wc -l)" 
    if (( fstab_efi_cnt != 1 )); then
        echo "There are no conforming /efi lines in fstab."
        echo "Not working on backing up EFI."
    else
        efi_device="$(grep '/efi' /etc/fstab | awk '$0 !~ /^\s*#/ {print $1}')"
        if [[ -n "$efi_device" ]]; then
            efi_mnt_pt="/mnt/tmp_mount_of_efi_for_backup"
            echo "Mounting boot device: $efi_device to $efi_mnt_pt"
              if mount --mkdir "$efi_device" "$efi_mnt_pt"; then
                echo "Creating backup of efi in ${root_pre_snapshot_mnt}/efi_on_root"
                rm -fR "${root_pre_snapshot_mnt}/efi_on_root"
                cp -a "$efi_mnt_pt" "${root_pre_snapshot_mnt}/efi_on_root"
                  echo "Unmounting $efi_mnt_pt"
                  umount "$efi_mnt_pt" && rm -R "$efi_mnt_pt"
            else
                echo -n "EFI device $efi_device did not mount, there will be no copy at "
                echo "/efi_on_root in snapshot"
                exit_code+=11
            fi
        fi
    fi
    echo "Unmounting $root_pre_snapshot_mnt"
    umount "$root_pre_snapshot_mnt"
fi

################
# SNAPSHOT
#
snapshot_volume="${volume}-snapshot"
if lvs "$volume_group" | grep -q "$snapshot_volume"; then
    echo "Volume $volume_group already has a snapshot of $volume"
    exit_code+=17
else
    echo "Creating lvm snapshot of $volume on $volume_group"
    /usr/sbin/lvcreate --snapshot \
        --permission r \
        --extents 100%FREE \
        --name "$snapshot_volume" \
        "${volume_group}/${volume}"
    echo "Snapshot created successfully"
fi

###############################
# SYSTEMD TARGET FOR NEXT BOOT
#
original_target=""
requires_count_in_target="$(systemctl show "$snapshot_target" | grep -c '^Requires')"
if (( requires_count_in_target > 1 )); then
    echo "There is more than one Requires= dependency in $snapshot_target"
    echo "This probably means that you overrode it with a drop in file rather than fully"
    echo "As replacing a dependency can only be done by overriding the entire unit file"
    echo " you will have to use systemctl edit --full $snapshot_target"
    echo "See the README for more information."
    echo "The snapshot will be created but you need to clean up before next reboot."
    exit_code+=3
else
    original_target="$(systemctl show "$snapshot_target" | \
        sed -nr 's/^Requires=(.*)$/\1/p')"
    if ! systemctl cat "$original_target" > /dev/null; then
        echo "Failed to parse: $original_target as extracted from"
        echo "  => systemctl show $snapshot_target"
        systemctl show "$snapshot_target" | grep '^Requires='
        exit_code+=5
    fi
    current_target="$(systemctl get-default)"
    if [[ "$current_target" == "$snapshot_target" ]]; then
      echo "Default systemd target is \"$snapshot_target\", reverting to $original_target"
      systemctl set-default "$original_target"
    elif [[ "$current_target" == "$original_target" ]]; then
      echo "Default systemd target already is $original_target, no need to change it."
    else
      echo "Default target is neither original one, nor snapshot maker, leaving unchanged."
      exit_code+=19
    fi
fi
