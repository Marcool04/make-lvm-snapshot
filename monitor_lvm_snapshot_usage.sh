#!/usr/bin/env bash

if (( UID != 0 )); then
  echo "This script must be run as root"
  exit 20
fi

set -eu

trap 'trap INT ; kill $$' INT TERM

env_file="/etc/default/make-lvm-snapshot"

usage() {
  echo "Usage: $(basename "$0") [-t threshold] [-c critical_threshold] [-i check_interval] [-q] [-?]" 1>&2
  echo "   -t threshold           -> threshold over witch to notify (default: 50%)" 1>&2
  echo "   -c critical_threshold  -> critical threshold over which to delete snapshot (default: 90%)." 1>&2
  echo "   -i check_interval      -> time in seconds to wait between two checks (default: 120)." 1>&2
  echo "   -q                     -> quiet, reduce output (default: false)." 1>&2
  echo "   -?                     -> display this help message." 1>&2
  echo "Following environment variables can also be used to set options:" 1>&2
  echo "   MLS_MONITOR_THRESHOLD" 1>&2
  echo "   MLS_MONITOR_CRITICAL_THRESHOLD" 1>&2
  echo "   MLS_MONITOR_CHECK_INTERVAL" 1>&2
  echo "Command line options take precedence over env vars." 1>&2
  echo "Environment file $env_file is sourced at script start." 1>&2
}

if [[ -e "$env_file" ]]; then
  # shellcheck source=/dev/null
  source "$env_file"
fi
threshold="${MLS_MONITOR_THRESHOLD:-50}"
critical_threshold="${MLS_MONITOR_CRITICAL_THRESHOLD:-90}"
check_interval="${MLS_MONITOR_CHECK_INTERVAL:-120}"
quiet="FALSE"
while getopts "t:c:i:q?" opt; do
  case $opt in
    t) threshold="$OPTARG"
    ;;
    c) critical_threshold="$OPTARG"
    ;;
    i) check_interval="$OPTARG"
    ;;
    q) quiet="TRUE"
    ;;
    ?) usage
       exit 0
    ;;
    *) echo "Failed to parse command line options."
       usage
       exit 2
    ;;
  esac
done
shift $((OPTIND-1))

if ((threshold>critical_threshold)); then
  echo "We have threshold > critical_threshold ($threshold > $critical_threshold)"
  echo "That doesn't make much sense..."
  usage
  exit 2
fi

if ! [[ "$check_interval" =~ ^[0-9]+$ ]]; then
  echo "This option provided as check interval is not an integer: $check_interval"
  usage
  exit 2
fi

function notify-send_root() {
    urgency="$1"
    title="$2"
    message="$3"
    if command notify-send 2&> /dev/null; then
      # From https://stackoverflow.com/a/49533938
      local display=
      display=":$(find /tmp/.X11-unix/* | sed 's#/tmp/.X11-unix/X##' | head -n 1)"
      local user=
      user="$(who | grep "(${display})" | awk '{print $1}' | head -n 1)"
      local uid
      uid="$(id -u "$user")"
      sudo -u "$user" \
           DISPLAY="$display" \
           DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$uid/bus" \
           notify-send --urgency="$urgency" --app-name="$title" "$message"
      return
    fi
    # Fallback to wall on text only systems, or ones with no notify framework
    wall "$title ||| $message"
}


while true; do
  readarray -t snapshots_jsons < <(\
    lvs --reportformat json | \
    jq --compact-output '.report[].lv[] | select(.origin != "")' \
  )
  if [[ "${#snapshots_jsons[@]}" == 0 ]]; then
    echo "There are no snapshots on this machine."
    echo "Exiting cleanly so systemd doesn't restart us."
    exit 0
  fi
  for snapshot_json in "${snapshots_jsons[@]}"; do
    snapshot_volume="$(jq --raw-output '.lv_name' <<< "$snapshot_json")"
    snapshot_volume_group="$(jq --raw-output '.vg_name' <<< "$snapshot_json")"
    snapshot_device="/dev/${snapshot_volume_group}/${snapshot_volume}"
    used="$(jq --raw-output '.data_percent' <<< "$snapshot_json")"
    if [[ "$(echo "$used > $critical_threshold" | bc -l)" == 1 ]]; then
      echo "ALERT, usage $used% greater than $critical_threshold% critical threshold on lvsnapshot!"
      echo -e "\a"
      notify-send_root \
        critical \
        "LVM snapshot CRITICALY filling up!" \
        "The LVM snapshot is at $used, over $critical_threshold CRITICAL threshold! Removing snapshot."
      if [[ -b "$snapshot_device" ]]; then
        echo "Running: lvremove --yes \"$snapshot_device\""
        if lvremove --yes "$snapshot_device"; then
          echo "Snapshot removed successfully."
        else
          echo "Failed to remove snapshot, this is BAAAD!"
          notify-send_root \
            critical \
            "LVM snapshot FAILED TO REMOVE!" \
            "Removal of LVM snapshot $snapshot_device FAILED. Now you're in trouble."
        fi
      else
        echo "$snapshot_device is an invalid block device. Can't unmount."
      fi
    elif [[ "$(echo "$used > $threshold" | bc -l)" == 1 ]]; then
      echo "ALERT, usage $used% greater than $threshold% threshold on lvsnapshot!"
      echo -e "\a"
      notify-send_root \
        critical \
        "LVM snapshot filling up!" \
        "The LVM snapshot is at $used, over $threshold threshold!"
    else
      if [[ "${quiet}" == "FALSE" ]]; then
        echo "$used% used on LVM snapshot $snapshot_volume, no problem there (threshold $threshold%)."
      fi
    fi
  done
  sleep "$check_interval"
done
